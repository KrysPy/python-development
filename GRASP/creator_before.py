from dataclasses import dataclass, field
from datetime import datetime


@dataclass
class ProductDescription:
    price: int
    description: str


@dataclass
class SaleLineItem:
    product: ProductDescription
    quantiy: int


@dataclass
class Sale:
    items: list[SaleLineItem] = field(defauld_factory=datetime.now)
    time: datetime = field(default=datetime.now())