from fastapi import FastAPI, Path, Query, HTTPException
from starlette import status

from book import Book, BookRequest


app = FastAPI()

BOOKS = [
    Book(1, 'Computer Science Pro', 'codingwithroby', 'A very nice book!', 5, 2010),
    Book(2, 'Be Fast with FastAPI', 'codingwithroby', 'A great book!', 5, 2022),
    Book(3, 'Master Endpoints', 'codingwithroby', 'A awesome book!', 5, 2033),
    Book(4, 'HP1', 'Author 1', 'Book Description', 2, 2010),
    Book(5, 'HP2', 'Author 2', 'Book Description', 3, 2011),
    Book(6, 'HP3', 'Author 3', 'Book Description', 1, 2009),
]


def find_book_id(book: Book):
    book.id = 1 if len(BOOKS) == 0 else BOOKS[-1].id + 1
    return book


@app.get("/books", status_code=status.HTTP_200_OK)
async def read_all_books() -> list:
    return BOOKS


# validation in path parameters added by adding Path as a assigned value
@app.get("/books/{book_id}", status_code=status.HTTP_200_OK)
async def read_book(book_id: int = Path(gt=0)):
    for book in BOOKS:
        if book.id == book_id:
            return book
    raise HTTPException(status_code=404, detail="Book cannot be found")


@app.get("/books/", status_code=status.HTTP_200_OK)
async def read_book_by_rating(rating: int = Query(gt=0, lt=6)):
    books_to_return = []
    for book in BOOKS:
        if book.rating == rating:
            books_to_return.append(book)
    return books_to_return


# validation for query parameters
@app.get("/books/publish/", status_code=status.HTTP_200_OK)
async def read_book_by_published_year(published_year: int = Query(gt=1999, lt=2031)):
    books_to_return = []
    for book in BOOKS:
        if book.published_year == published_year:
            books_to_return.append(book)
    return books_to_return


@app.post("/create-book", status_code=status.HTTP_201_CREATED)
async def create_book(book_request: BookRequest):
    book = Book(**book_request.dict())
    BOOKS.append(find_book_id(book))


@app.put("/books/update_book", status_code=status.HTTP_204_NO_CONTENT)
async def update_book(book: BookRequest):
    book_changed = False
    for i in range(len(BOOKS)):
        if BOOKS[i].id == book.id:
            BOOKS[i] = book
            book_changed = True

    if not book_changed:
        raise HTTPException(status_code=404, detail="Book cannot be found")


@app.delete("/books/{book_id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_book(book_id: int = Path(gt=0)):
    book_deleted = False
    for i in range(len(BOOKS)):
        if BOOKS[i].id == book_id:
            BOOKS.pop(i)
            book_deleted = True
            break

    if not book_deleted:
        raise HTTPException(status_code=404, detail="Book cannot be found")
