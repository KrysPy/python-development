from typing import Optional

from pydantic import BaseModel, Field


class Book:
    id: int
    title: str
    author: str
    description: str
    rating: int
    published_year: int

    def __init__(
        self, id: int, title: str, author: str, description: str, rating: int, published_year: int
        ) -> None:
        self.id = id
        self.title = title
        self.author = author
        self.description = description
        self.rating = rating
        self.published_year = published_year


class BookRequest(BaseModel):
    id: Optional[int] = Field(title='id is not needed')
    title: str = Field(min_length=3)
    author: str = Field(min_length=1)
    description: str = Field(min_length=1, max_length=100)
    rating: int = Field(gt=-1, lt=6)
    published_year: int

    class Config:
        schema_extra = {
            'example': {
                'title': 'A new book',
                'author': 'New author',
                'description': 'A new description of a book',
                'rating': 5,
                'published_year': 2023,
            }
        }
