import json

import pika

conn = pika.BlockingConnection(pika.ConnectionParameters("localhost"))
channel = conn.channel()

queue = channel.queue_declare("order_notify")
queue_name = queue.method.queue

channel.queue_bind(exchange="order", queue=queue_name, routing_key="order.notify")


def callback(ch, method, _, body):
    payload = json.loads(body)
    print(f"  [x]  Notifying {payload['user_email']}")
    print("  [x]  Done")
    # below is basic acknowledgement - so we're basically sending ack to rabbitMQ that the message was successfully
    # received and processed and rabbitMQ is free to remove this message
    # if e.g. out consumer dir for some reason, rabbitMQ do not delete this message, and we will not lose it
    ch.basic_ack(delivery_tag=method.delivery_tag)


channel.basic_consume(on_message_callback=callback, queue=queue_name)

print("  [x]  Waiting for notify message...")
channel.start_consuming()
