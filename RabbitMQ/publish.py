import json
import uuid

import pika

conn = pika.BlockingConnection(pika.ConnectionParameters("localhost"))
channel = conn.channel()

channel.exchange_declare(exchange="order", exchange_type="direct")
order = {
    "id": str(uuid.uuid4()),
    "user_email": "user@example.com",
    "product": "Rubber duck",
    "quantity": 1,
}

channel.basic_publish(
    exchange="order",
    routing_key="order.notify",
    body=json.dumps({"user_email": order["user_email"]}),
)
print("  [x]  Sent notify message")

channel.basic_publish(
    exchange="order", routing_key="order.report", body=json.dumps(order)
)
print("  [x]  Sent report message")

conn.close()
